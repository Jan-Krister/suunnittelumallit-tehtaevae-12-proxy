package proxy;

import java.util.ArrayList;
import java.util.List;

public class Main {

	public static void main(final String[] arguments) {

		List<Image> listA = new ArrayList<Image>();
		Image image1 = new ProxyImage("KikkelisKokkelis");
		Image image2 = new ProxyImage("HölkynKölkyn");

		listA.add(image1);
		listA.add(image2);

		for (Image i : listA) {
			System.out.println(i.showData());
		}

		for (Image i : listA) {
			i.displayImage();
		}

		for (Image i : listA) {
			i.displayImage();
		}

	}

}
